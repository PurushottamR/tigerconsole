﻿using Amazon.EC2;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TigerConsole
{
    class Program
    {
        private static bool bSucceeded;

        static int Main(string[] args)
        {

            Console.WriteLine("TIGER COMMAND RECEIVER");
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.WriteLine("Machine Name "  + ConfigurationManager.AppSettings["MachineName"].ToString());
            Console.WriteLine("Version " + ConfigurationManager.AppSettings["Version"].ToString());
            Console.WriteLine("Release " + ConfigurationManager.AppSettings["Release"].ToString());
            Console.WriteLine("--------------------------------------------------------------------------");
            int port = Convert.ToInt32( ConfigurationManager.AppSettings["ListningPort"].ToString());


             
            AsynchronousSocketListener.StartListening(port);
            return 0;


        }






    }
}
