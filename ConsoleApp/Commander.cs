﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TigerConsole
{
    public static class Commander
    {

        public static bool isBusy;
        public static string username;
        public static void StartWork(string Command)
        {
            if (isBusy)
            {
                return;
            }
            else
            {
                isBusy = true;
                username = Command.Split('#')[0];
                string action = Command.Split('#')[0];

                Task.Factory.StartNew(() => ThreadAWork(action));

                
            }

        }

        private static void ThreadAWork(string action)
        {
            Console.WriteLine("Thread:B Started");
            Thread.Sleep(36000);            
            
            
            
            Console.Clear();
            isBusy = false;
            Console.WriteLine("Thread:B Stopped");
        }

    }
}
